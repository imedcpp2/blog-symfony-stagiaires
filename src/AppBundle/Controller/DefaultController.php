<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Stagaires;
use AppBundle\Entity\user;
use AppBundle\Form\userType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;





class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig');
    }
    /**
     * @Route("/presentation", name="pres")
     */
    public function presAction()
    {
        // replace this example code with whatever you need
        return $this->render('default/presentation.html.twig');
    }
    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction()
    {
        // replace this example code with whatever you need
        return $this->render('default/contacte.html.twig');
    }
    /**
     * @Route("/donner", name="donner")
     */
    public function donnetAction()
    {
        $donnes = $this->getDoctrine()->getRepository('AppBundle:Stagaires');
        // $stagaires=$donnes->descDonnees(2);
        $stagaires=$donnes->findAll();
        // replace this example code with whatever you need
        return $this->render('default/donner.html.twig',['stagaires' => $stagaires]);
    }
    /**
     * @Route("/donner/{id}", name="donnersupp")
     */
    public function donnesuppAction($id)
    {
        
        $em = $this->getDoctrine()->getManager();        
        $donnes = $this->getDoctrine()->getRepository('AppBundle:Stagaires');
        $stagaire=$donnes->find($id);
        $em->remove($stagaire);
        $em->flush();



        $stagaires=$donnes->findAll();
        // replace this example code with whatever you need
        return $this->render('default/donner.html.twig',['stagaires' => $stagaires]);
    }
    /**
     * @Route("/edit/{id}", name="editer")
     */
    public function editeAction(Request $request,$id)
    {

        //recupération
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Stagaires');
        $stagiaire = $repository->find($id);

        //création de formulair
        $formBuilder = $this->createFormBuilder()
        ->add("nom",TextType::class,['data'=>$stagiaire->getNom()])
        ->add("prenom",TextType::class,['data'=>$stagiaire->getPrenom()])
        ->add("age",IntegerType::class,['data'=>$stagiaire->getAge()])
        ->add("email",TextType::class,['data'=>$stagiaire->getEmail()])
        ->add("adresse",TextType::class,['data'=>$stagiaire->getAdresse()])
        ->add("presence",CheckboxType::class,['data'=>$stagiaire->getPresence()])
        ->add("disc",TextType::class,['data'=>$stagiaire->getDisc()])
        ->add("Envoyer", SubmitType::class)
        ->getForm();
        $formBuilder->handleRequest($request);      
        if($formBuilder->isValid()){

            $donnes=$formBuilder->getData();           
            $stagiaire->setNom($donnes["nom"]);
            $stagiaire->setPrenom($donnes["prenom"]);
            $stagiaire->setAge($donnes["age"]);
            $stagiaire->setEmail($donnes["email"]);
            $stagiaire->setAdresse($donnes["adresse"]);
            $stagiaire->setPresence($donnes["presence"]);
            $stagiaire->setDisc($donnes["disc"]);
            $em = $this->getDoctrine()->getManager();   
            $em->persist($stagiaire);
            $em->flush(); //
            return $this->redirectToRoute('donner'); 

        }
        
       /* $stagiaire->setNom('Nom Modifier');
        $em->persist($stagiaire);
        $em->flush();
        return $this->redirectToRoute('donner');*/
        return $this->render('default/editer.html.twig',['form' => $formBuilder->createView()] );
       
    }

    /**
     * @Route("/user", name="user")
     */
    public function UserAction(Request $request)
    {
        //$user = new user();
        //$this->createFormBuilder();
        $form = $this->createForm(userType::class);
        return $this->render('default/user.html.twig',['form' => $form->createView()] );
    }


    /**
     * @Route("/ajout", name="ajouter")
     */
    public function AjoutAction(Request $request)
    {
        //$formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $advert);
        $stagiaire= new Stagaires();
        $form = $this->createFormBuilder( $stagiaire, array('attr' => array('novalidate' => 'novalidate')))
        ->add("nom",TextType::class,array('constraints' => array( new NotBlank, new Length(['min'=>2,'max'=>5])),
        'attr'=>['class'=>'form-control']))
        ->add("prenom",TextType::class,array('constraints' => array( new NotBlank),
        'attr'=>['class'=>'form-control']))
        ->add("age",IntegerType::class,array('constraints' => array( new NotBlank),
        'attr'=>['class'=>'form-control']))
        ->add("email", EmailType::class, array('constraints' => array(new NotBlank),
        'attr'=>['class'=>'form-control']))
        ->add("adresse",TextType::class,array('constraints' => array( new NotBlank),
        'attr'=>['class'=>'form-control']))
        ->add("presence",CheckboxType::class,array('constraints' => array( new NotBlank),'attr'=>['class'=>'form-check-input']))
        ->add("disc",TextType::class,array('constraints' => array( new NotBlank),
        'attr'=>['class'=>'form-control']))
        ->add("Envoyer", SubmitType::class,array('attr'=>['class'=>'btn-primary']))
        ->getForm();
        $form->handleRequest($request);        
        
        if($form->isValid()){

            $donnes=$form->getData();            
            // $stagiaire->setNom($donnes["nom"]);
            // $stagiaire->setPrenom($donnes["prenom"]);
            // $stagiaire->setAge($donnes["age"]);
            // $stagiaire->setEmail($donnes["email"]);
            // $stagiaire->setAdresse($donnes["adresse"]);
            // $stagiaire->setPresence($donnes["presence"]);
            // $stagiaire->setDisc($donnes["disc"]);
            $em = $this->getDoctrine()->getManager();   
            $em->persist($stagiaire);
            $em->flush(); //
            return $this->redirectToRoute('donner'); 

        }
        return $this->render('default/ajout.html.twig',['form' => $form->createView()] );
        //return $this->redirectToRoute('donner');        
        
    }
}
